#!/bin/zsh
# Grabbing the latest content from brwnrclse
rm -rf brwnrclse
git clone git@bitbucket.org:brwnrclse/brwnrclse --depth 1

mkdir -p public/scripts
cd brwnrclse
./utty
mv output/* ../public/scripts
ls -l ../public/scripts/*
cd ../
rm -rf brwnrclse
